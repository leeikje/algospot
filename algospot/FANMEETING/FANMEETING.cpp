#include <iostream>
#include <string>

using namespace std;

string fan, hyper;

// closed, open
int meeting(int start, int end)
{
	if (end - start < hyper.size())
		return 0;
	else
	{
		int mid = (start + end) / 2;
		int sum = meeting(start, mid) + meeting(mid, end);

		for (int i = mid - 1; start <= i && mid < i + hyper.size(); i--)
		{
			if (end < i + hyper.size())
				continue;

			bool pass = true;
			for (int j = 0; j < hyper.size(); j++)
				if (hyper[j] == 'M' && fan[i + j] == 'M')
				{
					pass = false;
					break;
				}

			if (pass)
				sum++;
		}

		return sum;
	}	
}

int main(void)
{
	int c;	

	cin >> c;
	for (int i = 0; i < c; i++)
	{
		cin >> hyper;
		cin >> fan;

		cout << meeting(0, fan.size()) << endl;
	}

	return 0;
}