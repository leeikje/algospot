#include <iostream>
#include <string>

using namespace std;

struct node {
	char v;
	node* child[4];

	~node() {
		for (int i = 0; i < 4; i++)
			if (child[i] != nullptr)
				delete child[i];
	}
};

node* makeQuadtree(string& line, int& p)
{
	node* result = new node();
	result->v = line[p++];
	if (result->v == 'x')
	{
		result->child[0] = makeQuadtree(line, p);
		result->child[1] = makeQuadtree(line, p);
		result->child[2] = makeQuadtree(line, p);
		result->child[3] = makeQuadtree(line, p);
	}

	return result;
}

void reverseHorizontal(node* quadtree)
{
	cout << quadtree->v;
	if (quadtree->v == 'x')
	{
		reverseHorizontal(quadtree->child[2]);
		reverseHorizontal(quadtree->child[3]);
		reverseHorizontal(quadtree->child[0]);
		reverseHorizontal(quadtree->child[1]);
	}
}

int main(void)
{
	int c;
	string line;

	cin >> c;
	for (int i = 0; i < c; i++)
	{
		cin >> line;

		int p = 0;
		node* quadtree = makeQuadtree(line, p);

		reverseHorizontal(quadtree);
		cout << endl;

		delete quadtree;
	}

	return 0;
}