#include <iostream>
#include <vector>

using namespace std;

// closed, closed
int cutMax(int* fence, int start, int end)
{
	if (start == end)
	{
		return fence[start];
	}
	else if (start > end)
	{
		return 0;
	}
	else
	{
		int mid = (start + end + 1) / 2;
		int leftMax = cutMax(fence, start, mid - 1);
		int rightMax = cutMax(fence, mid, end);

		int lp = mid - 1;
		int rp = mid;
		int sumMax = 0;
		int minHeight = 10000;

		while (start <= lp && rp <= end)
		{
			if (minHeight > fence[lp])
				minHeight = fence[lp];
			if (minHeight > fence[rp])
				minHeight = fence[rp];

			if (sumMax < minHeight * (rp - lp + 1))
				sumMax = minHeight * (rp - lp + 1);

			if (start <= lp - 1 && rp + 1 <= end)
			{
				if (fence[lp - 1] > fence[rp + 1])
					lp--;
				else
					rp++;
			}
			else if (start <= lp - 1)
			{
				lp--;
			}
			else if (rp + 1 <= end)
			{
				rp++;
			}
			else
			{
				break;
			}
		}

		if (sumMax < leftMax)
			sumMax = leftMax;
		if (sumMax < rightMax)
			sumMax = rightMax;

		return sumMax;
	}
}

int main(void)
{
	int c;
	int n;
	int fence[20000];

	cin >> c;
	for (int i = 0; i < c; i++)
	{
		cin >> n;
		for (int j = 0; j < n; j++)
			cin >> fence[j];
		
		cout << cutMax(fence, 0, n - 1) << endl;
	}

	return 0;
}